package com.example.android.kitchen;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.kitchen.Order;
import com.example.android.kitchen.R;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends ArrayAdapter<String> {

    private ArrayList<String> orders;


    public OrderAdapter(Context ct, ArrayList<String> orders) {
        super(ct, 0, orders);
        this.orders = orders;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        String order = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.order, parent, false);
        }
        TextView tvName = convertView.findViewById(R.id.tv_order_name);
        final Button btnDone = convertView.findViewById(R.id.btn_done_order);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(position);
                OrderAdapter.this.notifyDataSetChanged();
                btnDone.setText("DONE");
                btnDone.setEnabled(false);
            }
        });
        String display= "Table: "+order;
        tvName.setText(display);
        return convertView;
    }

    private void sendBroadcast(int position) {
        Intent intent = new Intent();
        intent.setAction("com.example.android.kitchen");
        intent.putExtra("index",position);
        getContext().sendBroadcast(intent);
    }
}
