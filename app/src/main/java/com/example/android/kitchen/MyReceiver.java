package com.example.android.kitchen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


//receiver from restaurant
public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context ct, Intent intent) {

         Bundle b = intent.getExtras();
         System.out.println(intent.getAction());
         if(intent.getAction().equals("com.example.android.restaurant_call")) {

             System.out.println("XXX");
             Intent it2 = new Intent("call-staff");
             it2.putExtra("call",b.getString("CALL"));
             ct.sendBroadcast(it2);
         } else {
             System.out.println("RECEive ORder");
             Intent it = new Intent("receive-order");
             it.putStringArrayListExtra("receive", b.getStringArrayList("list-order"));
             ct.sendBroadcast(it);

         }
    }
}
