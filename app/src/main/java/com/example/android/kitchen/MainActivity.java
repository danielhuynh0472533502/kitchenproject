package com.example.android.kitchen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    GridView gridOrder;
    OrderAdapter adapter;
    ArrayList<String> listOrder;
    LinearLayout staffNeeds;
    Button btnTable;
    TextView tvTable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridOrder = findViewById(R.id.grid_order);
        staffNeeds= findViewById(R.id.staff_needs);
        btnTable = findViewById(R.id.btn_table);
        tvTable = findViewById(R.id.tv_table);
        listOrder= new ArrayList<>();
        adapter = new OrderAdapter(this,listOrder);
        gridOrder.setAdapter(adapter);
        registerReceiver(broadcastReceiver,new IntentFilter("receive-order"));
        registerReceiver(broadcastReceiver2,new IntentFilter("call-staff"));
        //set action
        btnTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                staffNeeds.setVisibility(LinearLayout.GONE);
            }
        });

    }
    //receive order from my receiver
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            ArrayList<String> newOrder = b.getStringArrayList("receive");
            listOrder.addAll(newOrder);
            System.out.println(listOrder);
            adapter.notifyDataSetChanged();

        }
    };

    //call staff
    BroadcastReceiver broadcastReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            tvTable.setText("Table "+b.getString("call"));
            staffNeeds.setVisibility(LinearLayout.VISIBLE);


        }
    };



}
